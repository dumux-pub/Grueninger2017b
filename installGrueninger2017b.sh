#!/usr/bin/env bash

# We provide this script to download and patch all Dune modules for Grueninger2017b
#
# How to procede:
# 1. create an emtpy directory
# 2. copy this file into directory and execute it,
#    maybe you have to set the executable bit first
# 3. check the README.md to get the location of the executables

###
# download Dune modules
###
echo "###"
echo "# download Dune modules"
echo "###"

# this module
# must be checkout first to provide patches
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Grueninger2017b.git

# dune-common
# releases/2.4 # e1a9b914d0a3b133641647a6987c61c9e2a5423a # 2016-06-15 07:05:39 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.4
git reset --hard e1a9b914d0a3b133641647a6987c61c9e2a5423a
cd ..

# dune-geometry
# releases/2.4 # ac1fca4ff249ccdc7fb035fa069853d84b93fb73 # 2016-05-28 07:30:48 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.4
git reset --hard ac1fca4ff249ccdc7fb035fa069853d84b93fb73
cd ..

# dune-grid
# releases/2.4 # c8ab7130cd57b14c88194cd8ed52a6ebefe64594 # 2016-06-16 08:28:26 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.4
git reset --hard c8ab7130cd57b14c88194cd8ed52a6ebefe64594
patch -p1 < ../Grueninger2017b/patches/dune-grid_9999-uncommitted-changes.patch
cd ..

# dune-localfunctions
# releases/2.4 # b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a # 2016-05-28 07:35:51 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.4
git reset --hard b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a
cd ..

# dune-istl
# releases/2.4 # ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff # 2016-07-30 12:35:06 +0200 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.4
git reset --hard ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff
cd ..

# dune-typetree
# releases/2.3 # ecffa10c59fa61a0071e7c788899464b0268719f # 2014-07-23 17:49:24 +0200 # Steffen Müthing
git clone https://gitlab.dune-project.org/staging/dune-typetree.git
cd dune-typetree
git checkout releases/2.3
git reset --hard ecffa10c59fa61a0071e7c788899464b0268719f
cd ..

# dune-pdelab
# releases/2.0 # 19c782eea7232e94849617b20dfee8d9781eb4fb # 2016-09-07 07:09:49 # Christian Engwer
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
cd dune-pdelab
git checkout releases/2.0
git reset --hard 19c782eea7232e94849617b20dfee8d9781eb4fb
patch -p1 < ../Grueninger2017b/patches/dune-pdelab_9999-ignore-PETSc.patch
cd ..

# dune-multidomaingrid
# releases/2.3 # 3b829b7a130473749b8af2d402eaef1eff1071a7 # 2016-09-26 16:55:13 +0200 # Steffen Müthing
git clone https://github.com/smuething/dune-multidomaingrid.git
cd dune-multidomaingrid
git checkout releases/2.3
git reset --hard 3b829b7a130473749b8af2d402eaef1eff1071a7
cd ..

# dune-multidomain
# releases/2.0 # e3d52982dc9acca9bf13cd8f77bf0329c61b6327 # 2016-09-26 15:13:50 +0200 # Steffen Müthing
git clone https://github.com/smuething/dune-multidomain.git
cd dune-multidomain
git checkout releases/2.0
git reset --hard e3d52982dc9acca9bf13cd8f77bf0329c61b6327
cd ..

# dumux
# releases/2.10 # 9a73ab52cd278b5f148328529edafbab1151dfb3 # 2016-11-21 09:49:47 +0100 # Martin Schneider
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/2.10
git reset --hard 9a73ab52cd278b5f148328529edafbab1151dfb3
cd ..

###
# configure and build modules
###
echo "###"
echo "# configure and build modules"
echo "###"

./dune-common/bin/dunecontrol --opts=dumux/optim.opts all

