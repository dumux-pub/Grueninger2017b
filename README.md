Summary
=======
This is the Dune module containing the code for producing the results
intended for publication:

*Coupling DuMuX and DUNE-PDELab to investigate evaporation
at the interface between Darcy and Navier-Stokes flow.  
C. Grüninger, T. Fetzer, B. Flemisch, R. Helmig.  
SimTech Technical Report 2017 - 1.  
[doi: 10.18419/opus-9360](http://dx.doi.org/10.18419/opus-9360)*

[BibTeX entry](grueninger2017b.bib)


Two sparse matrices from a 2d and a 3d setup were submitted to Tim Davis'
[SuiteSparse Matrix collection](https://sparse.tamu.edu/)
and are listed as
[2814 Grueninger/windtunnel_evap2d](https://sparse.tamu.edu/Grueninger/windtunnel_evap2d)
and
[2815 Grueninger/windtunnel_evap3d](https://sparse.tamu.edu/Grueninger/windtunnel_evap3d).


Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and to execute the file
[installGrueninger2017b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Grueninger2017b/raw/master/installGrueninger2017b.sh)
in this directory.

```bash
mkdir -p Grueninger2017b && cd Grueninger2017b
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Grueninger2017b/raw/master/installGrueninger2017b.sh
chmod u+x installGrueninger2017b.sh
bash ./installGrueninger2017b.sh
```

You need to have installed at least:
* CMake 2.8.12
* C, C++ compiler (GCC 4.8 or Clang 3.5 should be enough, the newer the better)
* Fortran compiler (gfortran)
* SuperLU or UMFPack from SuiteSparse
* Boost

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The applications used for this publication can be found in
`appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test`.
There are tests which can be run using CTest to check whether the reference
results are obtained. To run the tests, compile all applications and run CTest
from the build directory
```bash
make build_tests
ctest
```
The results may vary depending on the compiler and the processor, especially
the box examples contain oscillations which can cause error peakes in
comparison with the provided reference solutions.

* __test_conservation__:
  Single-phase isothermal example to show the mass conservation across the interface.
  Above the interface is the free flow, below the porous medium, cf. section 4.1.
* __test_conservation_box__:
  Conversation test with box/box discretization, see section 4.1.
* __windtunnel__:
  The basic setup of the wind tunnel and the sand box below from section 4.2.
* __windtunnel_box__:
  Wind tunnel with box/box discretization for comparison, section 4.2.
* __windtunnel3d__:
  Three-dimensional wind tunnel setup from section 4.3. Can take several hours
  to complete.

In order to run an executable use the script executeGrueninger2017b:
```bash
./executeGrueninger2017b windtunnel
```


Further ressources
==================

In the folder `further_ressources` are some of the files we used to generate the
images for the article.
* VTK files with according state files for visualization with ParaView.
* Gnuplot files to generate the plots.
* Jobfiles we used to generate the evaporation rates for Gnuplot. We do not
  provide a ready-to-use script as the calculation takes several hours for
  multiple setups. Inside the directory is also a table of the used grading
  factors for the grid.


Used Dune module versions
=========================

The content of this DUNE module was extracted from the module dumux-devel.
In particular, all headers in dumux-devel that are required to build the
executables from the sources

  appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel.cc,
  appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel_box.cc,
  appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/test_conservation_box.cc,
  appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/test_conservation.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

Used DUNE modules, their version / Git hash and additional patches

```
dune-common               releases/2.4    e1a9b914d0a3b133641647a6987c61c9e2a5423a
dune-geometry             releases/2.4    ac1fca4ff249ccdc7fb035fa069853d84b93fb73
dune-grid                 releases/2.4    c8ab7130cd57b14c88194cd8ed52a6ebefe64594
    with patch dune-grid_9999-uncommitted-changes.patch
dune-localfunctions       releases/2.4    b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a
dune-istl                 releases/2.4    ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff
dune-typetree             releases/2.3    ecffa10c59fa61a0071e7c788899464b0268719f
dune-pdelab               releases/2.0    19c782eea7232e94849617b20dfee8d9781eb4fb
dune-multidomaingrid      releases/2.3    3b829b7a130473749b8af2d402eaef1eff1071a7
dune-multidomain          releases/2.0    e3d52982dc9acca9bf13cd8f77bf0329c61b6327
dumux                     releases/2.10   9a73ab52cd278b5f148328529edafbab1151dfb3
```
