// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/** \file
 *  \brief Constraints fixing the pressure value in cells, having boundaries
 *         with edges in the specified domain.
 *
 * \note This is only needed in cases were no velocity outflow boundary
 * condition is set. For outflow the pressure is evaluated at the
 * boundary.
*/

#ifndef DUMUX_NAVIER_STOKES_FIXPRESSURECONSTRAINTS_HH
#define DUMUX_NAVIER_STOKES_FIXPRESSURECONSTRAINTS_HH

#include <dune/common/version.hh>

#include <dune/grid/common/grid.hh>

#include <dune/pdelab/common/geometrywrapper.hh>
#include <dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>

#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproperties.hh>

namespace Dumux {

/**
  * \brief Sets fix values for the specified domain.
  *
  * \tparam TypeTag TypeTag of the problem
  */
template <class TypeTag>
class FixPressureConstraints
{
public:
    enum { doBoundary = false };
    enum { doProcessor = false };
    enum { doSkeleton = false };
    enum { doVolume = true };

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    enum { dim = GridView::dimension };

    //! \brief Constructor
    //! \tparam GridView GridView type
    FixPressureConstraints(GridView gv_, Problem& problem)
        : gv(gv_), problemPtr_(0)
    {
        problemPtr_ = &problem;
    }

    /**
      * \brief Volume constraints to fix specified elements to a given value.
      *
      * \tparam EG  element geometry
      * \tparam LFS local function space
      * \tparam T   transformation type
      */
#if DUNE_VERSION_NEWER(DUNE_PDELAB, 2, 4)
    template<typename Parameter, typename EG, typename LFS, typename T>
    void volume(const Parameter&, const EG& eg, const LFS& lfs, T& trafo) const
#else
    template<typename EG, typename LFS, typename T>
    void volume(const EG& eg, const LFS& lfs, T& trafo) const
#endif
    {
        if (GET_PROP_VALUE(TypeTag, FixPressureConstraints))
        {
            typedef typename GridView::IntersectionIterator IntersectionIterator;
            IntersectionIterator endIt = gv.iend(eg.entity());
            for (IntersectionIterator it = gv.ibegin(eg.entity()); it != endIt; ++it)
            {
                if (it->boundary())
                {
                    // whether the current intersection has a Dirichlet value at one of its corners
                    bool dirichletCorner = false;
                    // we have 2^(dim-1) corners, check each
                    // each bit is the value of one dimension, e.g. 6 = 0b110 => (0.0, 1.0, 1.0)
                    for (unsigned int curCorner = 0; curCorner < std::pow(2, dim-1); ++curCorner)
                    {
                        Dune::FieldVector<Scalar, dim-1> faceCornerLocal(0.0);
                        if (dim-1 >= 1)
                        {
                            faceCornerLocal[0] = curCorner % 2;
                        }
                        if (dim-1 >= 2)
                        {
                            faceCornerLocal[1] = (curCorner / 2 ) % 2;
                        }
                        //! \todo the isDirichlet() from BCPressure should be called (but had Problem with correct initialization)
                        DimVector global = it->geometry().global(faceCornerLocal);
                        dirichletCorner |= problem_().bcPressureIsDirichlet(global);
              //           dirichletCorner |= bcPressure.isDirichlet(*it, faceCornerLocal);
                    }

                    if (dirichletCorner)
                    {
                        // empty map means Dirichlet constraint
                        typename T::RowType empty;

                        // set first (and only because we have p0 elements) degrees of freedom
                        trafo[lfs.dofIndex(0)] = empty;
                    }
                }
            }
        }
    }

private:
    GridView gv;

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};
} // end namespace Dumux

#endif // DUMUX_NAVIER_STOKES_FIXPRESSURECONSTRAINTS_HH
