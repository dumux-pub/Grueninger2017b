// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Non-isothermal two-component Stokes subproblem with air flowing
 *        from the left to the right and coupling at the bottom.
 */
#ifndef DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_BOX_HH
#define DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_BOX_HH

#include <dumux/freeflow/stokesncni/model.hh>
#include <dumux/multidomain/subdomainpropertydefaults.hh>
#include <dumux/multidomain/2cnistokes2p2cni/stokesncnicouplinglocalresidual.hh>

namespace Dumux
{

template <class TypeTag>
class FreeFlowSubProblem;

namespace Properties
{
NEW_TYPE_TAG(FreeFlowSubProblem,
             INHERITS_FROM(BoxStokesncni, SubDomain));

// Set the problem property
SET_TYPE_PROP(FreeFlowSubProblem, Problem, Dumux::FreeFlowSubProblem<TypeTag>);

// Use the StokesncniCouplingLocalResidual for the computation of the local residual in the free-flow domain
SET_TYPE_PROP(FreeFlowSubProblem, LocalResidual, StokesncniCouplingLocalResidual<TypeTag>);

// Used the fluid system from the coupled problem
SET_TYPE_PROP(FreeFlowSubProblem, FluidSystem,
              typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag), FluidSystem));

// Disable use of mole formulation
SET_BOOL_PROP(FreeFlowSubProblem, UseMoles, false);

// Disable gravity
SET_BOOL_PROP(FreeFlowSubProblem, ProblemEnableGravity, false);

// switch inertia term on or off
#if STOKES
SET_BOOL_PROP(FreeFlowSubProblem, EnableNavierStokes, false);
#else
SET_BOOL_PROP(FreeFlowSubProblem, EnableNavierStokes, true);
#endif

// Disable symmetrized velocity gradient
SET_BOOL_PROP(FreeFlowSubProblem, EnableUnsymmetrizedVelocityGradient, true);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief Stokes2cni problem with air flowing from the left to the right.
 *
 * \todo update test description
 * This sub problem uses the \ref StokesNCNIModel. It is part of the 2cnistokes2p2cni model and
 * is combined with the 2p2csubproblem for the Darcy domain.
 */
template <class TypeTag>
class FreeFlowSubProblem : public StokesProblem<TypeTag>
{
    typedef FreeFlowSubProblem<TypeTag> ThisType;
    typedef StokesProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension
    };
    enum {
        // equation indices
        massBalanceIdx = Indices::massBalanceIdx,
        momentumXIdx = Indices::momentumXIdx, // Index of the x-component of the momentum balance
        momentumYIdx = Indices::momentumYIdx, // Index of the y-component of the momentum balance
        momentumZIdx = Indices::momentumZIdx, // Index of the z-component of the momentum balance
        transportEqIdx = Indices::transportEqIdx, // Index of the transport equation (massfraction)
        energyEqIdx = Indices::energyEqIdx     // Index of the energy equation (temperature)
    };
    enum { // primary variable indices
        pressureIdx = Indices::pressureIdx,
        velocityXIdx = Indices::velocityXIdx,
        velocityYIdx = Indices::velocityYIdx,
        velocityZIdx = Indices::velocityZIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        temperatureIdx = Indices::temperatureIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;


public:
    /*!
     * \brief The sub-problem for the Stokes subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    FreeFlowSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = positions0.front();
        bBoxMax_[0] = positions0.back();
        bBoxMin_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);
        bBoxMax_[1] = positions1.back();

        darcyXLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
        darcyXRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);

        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Velocity);
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Pressure);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MassMoleFrac);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Temperature);
    }

    /*!
     * \todo bBox functions have to be overwritten, otherwise they remain uninitialised
     */
    //! \copydoc BoxProblem::&bBoxMin()
    const GlobalPosition &bBoxMin() const
    { return bBoxMin_; }

    //! \copydoc BoxProblem::&bBoxMax()
    const GlobalPosition &bBoxMax() const
    { return bBoxMax_; }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name)
               + std::string("-ff_box");
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypesAtPos()
   void boundaryTypesAtPos(BoundaryTypes &values, const GlobalPosition &globalPos) const
    {
        values.setAllDirichlet();

        if (onUpperBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setNeumann(energyEqIdx);
        }

        if (onLowerBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setNeumann(energyEqIdx);

            if (globalPos[0] > darcyXLeft_ + eps_
                && globalPos[0] < darcyXRight_ - eps_)
            {
                values.setAllCouplingDirichlet();
                values.setCouplingNeumann(momentumXIdx);
                values.setCouplingNeumann(momentumYIdx);
            }
        }

        // Left inflow boundaries should be Neumann, otherwise the
        // evaporative fluxes are much more grid dependent
        if (onLeftBoundary_(globalPos))
        {
            values.setAllDirichlet();
        }

        // the mass balance has to be of type outflow
        // it does not get a coupling condition, since pn is a condition for stokes
        values.setOutflow(massBalanceIdx);

        // set pressure at one point, do NOT specify this
        // if the Darcy domain has a Dirichlet condition for pressure
        if (onRightBoundary_(globalPos))
        {
            values.setAllOutflow();
            values.setDirichlet(pressureIdx, massBalanceIdx);
            if (onUpperBoundary_(globalPos) || onLowerBoundary_(globalPos)) // corner points
            {
                values.setAllDirichlet();
                values.setOutflow(massBalanceIdx);
            }
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichletAtPos()
     void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0.0;

        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::neumannAtPos()
    void neumannAtPos(PrimaryVariables &values,const GlobalPosition &globalPos) const
    {
        values = Scalar(0);
    }

    // \}

    //! \copydoc Dumux::ImplicitProblem::sourceAtPos()
     void sourceAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = Scalar(0);
    }

    //! \copydoc Dumux::ImplicitProblem::initialAtPos()
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }
    // \}

private:
    /*!
     * \brief Internal method for the initial condition
     */
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[velocityXIdx] = velocity_;
        if (onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos))
        {
          values[velocityXIdx] = 0.0;
        }
        values[velocityYIdx] = 0.0;

        values[pressureIdx] = pressure_;
        values[massOrMoleFracIdx] = massMoleFrac_;
        values[temperatureIdx] = temperature_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;

    Scalar velocity_;
    Scalar pressure_;
    Scalar massMoleFrac_;
    Scalar temperature_;

    Scalar darcyXLeft_;
    Scalar darcyXRight_;
};
} //end namespace

#endif // DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_BOX_HH
