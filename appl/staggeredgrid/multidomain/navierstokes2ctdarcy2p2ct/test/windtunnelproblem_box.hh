// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem class for the coupling of a non-isothermal two-component
 *        free flow and and a non-isothermal two-phase two-component
 *        porous medium model.
 */
#ifndef DUMUX_WINDTUNNEL_PROBLEM_BOX_HH
#define DUMUX_WINDTUNNEL_PROBLEM_BOX_HH

#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/io/file/dgfparser.hh>

#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/multidomain/problem.hh>
#include <dumux/multidomain/2cstokes2p2c/newtoncontroller.hh>
#include <dumux/multidomain/2cnistokes2p2cni/localoperator.hh>
#include <dumux/multidomain/2cnistokes2p2cni/problem.hh>
#include <dumux/multidomain/2cnistokes2p2cni/propertydefaults.hh>

#include "windtunneldarcyspatialparams.hh"
#include "windtunneldarcysubproblem_box.hh"
#include "windtunnelstokessubproblem_box.hh"

namespace Dumux
{
template <class TypeTag>
class WindtunnelProblemBox;

namespace Properties
{
NEW_TYPE_TAG(WindtunnelProblemBox, INHERITS_FROM(TwoCNIStokesTwoPTwoCNI));

// Set the local coupling operator
SET_TYPE_PROP(WindtunnelProblemBox, MultiDomainCouplingLocalOperator,
              Dumux::TwoCNIStokesTwoPTwoCNILocalOperator<TypeTag>);

// Set the grid type
SET_TYPE_PROP(WindtunnelProblemBox, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Set the global problem
SET_TYPE_PROP(WindtunnelProblemBox, Problem, WindtunnelProblemBox<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(WindtunnelProblemBox, SubDomain1TypeTag, TTAG(FreeFlowSubProblem));
SET_TYPE_PROP(WindtunnelProblemBox, SubDomain2TypeTag, TTAG(PorousMediumSubProblem));

// Set the global problem in the context of the two sub-problems
SET_TYPE_PROP(FreeFlowSubProblem, MultiDomainTypeTag, TTAG(WindtunnelProblemBox));
SET_TYPE_PROP(PorousMediumSubProblem, MultiDomainTypeTag, TTAG(WindtunnelProblemBox));

// Set the other sub-problem for each of the two sub-problems
SET_TYPE_PROP(FreeFlowSubProblem, OtherSubDomainTypeTag, TTAG(PorousMediumSubProblem));
SET_TYPE_PROP(PorousMediumSubProblem, OtherSubDomainTypeTag, TTAG(FreeFlowSubProblem));

// Set the same spatial parameters for both sub-problems
SET_TYPE_PROP(PorousMediumSubProblem, SpatialParams, Dumux::WindtunnelSpatialParams<TypeTag>);

// Set the fluid system
SET_TYPE_PROP(WindtunnelProblemBox, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Use UMFPack solver
SET_TYPE_PROP(WindtunnelProblemBox, LinearSolver, UMFPackBackend<TypeTag>);

// Frequency of writing restart files
NEW_PROP_TAG(ProblemFreqRestart);
SET_INT_PROP(WindtunnelProblemBox, ProblemFreqRestart, 1e5);

// Frequency of writing output files
NEW_PROP_TAG(ProblemFreqOutput);
SET_INT_PROP(WindtunnelProblemBox, ProblemFreqOutput, 1);
}

/*!
 * \brief please doc me
 */
template <class TypeTag = TTAG(WindtunnelProblemBox) >
class WindtunnelProblemBox : public TwoCNIStokesTwoPTwoCNIProblem<TypeTag>
{
    typedef TwoCNIStokesTwoPTwoCNIProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    using ParameterTree = typename GET_PROP(TypeTag, ParameterTree);

    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum { dim = GridView::dimension };

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;

public:
    /*!
     * \brief The problem for the coupling of the free-flow transport and Darcy flow
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    template<class GridView>
    WindtunnelProblemBox(TimeManager &timeManager,
                         GridView gridView)
    : ParentType(timeManager, gridView)
    {
        // define location of the interface
        interfacePosY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);
        darcyXLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
        darcyXRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);

        // define output options
        freqRestart_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, FreqRestart);
        freqOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, FreqOutput);

        freeFlow_ = this->sdID1();
        porousMedium_ = this->sdID2();

        initializeGrid();

        // initialize the tables of the fluid system
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
     }


    /*!
     * \brief Initialization of the grids
     *
     * This function splits the multidomain grid in the two
     * individual subdomain grids and takes care of parallelization.
     */
    void initializeGrid()
    {
        MDGrid& mdGrid = this->mdGrid();
        mdGrid.startSubDomainMarking();

        // subdivide grid in two subdomains
        for (const auto& element : Dune::elements(mdGrid.leafGridView()))
        {
            // this is required for parallelization
            // checks if element is within a partition
            if (element.partitionType() != Dune::InteriorEntity)
                continue;

            GlobalPosition globalPos = element.geometry().center();

            if (globalPos[1] > interfacePosY_)
                mdGrid.addToSubDomain(freeFlow_, element);
            else
                if(globalPos[0] > darcyXLeft_ && globalPos[0] < darcyXRight_)
                    mdGrid.addToSubDomain(porousMedium_, element);
        }
        mdGrid.preUpdateSubDomains();
        mdGrid.updateSubDomains();
        mdGrid.postUpdateSubDomains();

        gridinfo(this->sdGrid1());
        gridinfo(this->sdGrid2());
    }

    //! \copydoc Dumux::CoupledProblem::postTimeStep()
    void postTimeStep()
    {
        // call the postTimeStep function of the subproblems
        this->sdProblem1().postTimeStep();
        this->sdProblem2().postTimeStep();
    }

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    //! \copydoc Dumux::CoupledProblem::shouldWriteRestartFile()
    bool shouldWriteRestartFile() const
    {
        return ((this->timeManager().timeStepIndex() > 0 &&
                (this->timeManager().timeStepIndex() % freqRestart_ == 0))
                // also write a restart file at the end of each episode
                || this->timeManager().episodeWillBeOver());
    }

    //! \copydoc Dumux::CoupledProblem::shouldWriteOutput()
    bool shouldWriteOutput() const
    {
        return (this->timeManager().timeStepIndex() % freqOutput_ == 0
                || this->timeManager().episodeWillBeOver());
    }

private:
    typename MDGrid::SubDomainType freeFlow_;
    typename MDGrid::SubDomainType porousMedium_;

    unsigned freqRestart_;
    unsigned freqOutput_;

    Scalar interfacePosY_;
    Scalar darcyXLeft_;
    Scalar darcyXRight_;
    Scalar episodeLength_;
};

} //end namespace

#endif // DUMUX_WINDTUNNEL_PROBLEM_BOX_HH
