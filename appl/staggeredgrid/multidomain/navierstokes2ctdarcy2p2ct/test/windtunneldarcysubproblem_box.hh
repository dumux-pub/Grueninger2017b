// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal two-phase two-component porous-medium subproblem
 *        with coupling at the top boundary.
 */
#ifndef DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_BOX_HH
#define DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_BOX_HH

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/multidomain/subdomainpropertydefaults.hh>
#include <dumux/multidomain/localoperator.hh>
#include <dumux/multidomain/2cnistokes2p2cni/2p2cnicouplinglocalresidual.hh>
#include <dumux/porousmediumflow/2p2c/implicit/indices.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>

#include "windtunneldarcyspatialparams.hh"

namespace Dumux
{
template <class TypeTag>
class PorousMediumSubProblem;

namespace Properties
{
NEW_TYPE_TAG(PorousMediumSubProblem,
             INHERITS_FROM(BoxTwoPTwoCNI, SubDomain, DarcySpatialParams));

// Set the problem property
SET_TYPE_PROP(PorousMediumSubProblem, Problem, PorousMediumSubProblem<TTAG(PorousMediumSubProblem)>);

// Use the 2p2cni local jacobian operator for the 2p2cniCoupling model
SET_TYPE_PROP(PorousMediumSubProblem, LocalResidual, TwoPTwoCNICouplingLocalResidual<TypeTag>);

// Choose pn and Sw as primary variables
SET_INT_PROP(PorousMediumSubProblem, Formulation, TwoPTwoCFormulation::pnsw);

// The gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(PorousMediumSubProblem, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

// Used the fluid system from the coupled problem
SET_TYPE_PROP(PorousMediumSubProblem,
              FluidSystem,
              typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag), FluidSystem));

// Somerton is used as model to compute the effective thermal heat conductivity
SET_TYPE_PROP(PorousMediumSubProblem, ThermalConductivityModel,
              ThermalConductivitySomerton<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Use formulation based on mass fractions
SET_BOOL_PROP(PorousMediumSubProblem, UseMoles, false);

// Enable/disable velocity output
SET_BOOL_PROP(PorousMediumSubProblem, VtkAddVelocity, true);

// Enable gravity
SET_BOOL_PROP(PorousMediumSubProblem, ProblemEnableGravity, true);

// Live plot of the evaporation rates
NEW_PROP_TAG(OutputPlotEvaporationRate);
NEW_PROP_TAG(OutputPlotStaggeredEvaporationRate);
SET_BOOL_PROP(PorousMediumSubProblem, OutputPlotEvaporationRate, false);
SET_BOOL_PROP(PorousMediumSubProblem, OutputPlotStaggeredEvaporationRate, false);

// Frequency of writing storage files
NEW_PROP_TAG(ProblemFreqMassOutput);
SET_INT_PROP(PorousMediumSubProblem, ProblemFreqMassOutput, 1);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief Non-isothermal two-phase two-component porous-medium subproblem
 *        with coupling at the top boundary.
 *
 * \todo please doc me
 *
 * This sub problem uses the \ref TwoPTwoCModel. It is part of the 2p2cni model and
 * is combined with a model for the free flow domain.
 */
template <class TypeTag = TTAG(PorousMediumSubProblem) >
class PorousMediumSubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Grid Grid;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    // the equation indices
    enum { contiTotalMassIdx = Indices::contiNEqIdx,
           contiWEqIdx = Indices::contiWEqIdx,
           energyEqIdx = Indices::energyEqIdx };
    // the indices of the primary variables
    enum { pressureIdx = Indices::pressureIdx,
           switchIdx = Indices::switchIdx,
           temperatureIdx = Indices::temperatureIdx };
    // grid dimension
    enum { dim = GridView::dimension };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;

public:
    /*!
     * \brief The sub-problem for the porous-medium subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    PorousMediumSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        darcyXLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
        darcyXRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = std::max(positions0.front(),darcyXLeft_);
        bBoxMax_[0] = std::min(positions0.back(),darcyXRight_);
        bBoxMin_[1] = positions1.front();
        bBoxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);

        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Pressure);
        switch_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Switch);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Temperature);
        initialPhasePresence_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, InitialPhasePresence);

        freqMassOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, FreqMassOutput);

        storageLastTimestep_ = Scalar(0);
        lastMassOutputTime_ = Scalar(0);

        outfile.open("storage_box.out");
        outfile << "Time[s]" << ";"
                << "TotalMassChange[kg/(s*mDepth)]" << ";"
                << "WaterMassChange[kg/(s*mDepth))]" << ";"
                << "IntEnergyChange[J/(m^3*s*mDepth)]" << ";"
                << "WaterMass[kg/mDepth]" << ";"
                << "WaterMassLoss[kg/mDepth]" << ";"
                << "EvaporationRate[mm/s]"
                << std::endl;
    }

    //! \brief The destructor
    ~PorousMediumSubProblem()
    {
        outfile.close();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc BoxProblem::&bBoxMin()
    const GlobalPosition &bBoxMin() const
    { return bBoxMin_; }

    //! \copydoc BoxProblem::&bBoxMax()
    const GlobalPosition &bBoxMax() const
    { return bBoxMax_; }

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name)
               + std::string("-pm_box");
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        ParentType::init();
        this->model().globalStorage(storageLastTimestep_);
        initialWaterContent_ = storageLastTimestep_[contiWEqIdx];
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypesAtPos()
    void boundaryTypesAtPos(BoundaryTypes &values, const GlobalPosition &globalPos) const
    {
        values.setAllNeumann();

        if (onUpperBoundary_(globalPos)
            &&  globalPos[0] > darcyXLeft_ + eps_
            && globalPos[0] < darcyXRight_ - eps_)
        {
            values.setAllCouplingNeumann();
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichletAtPos()
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::solDependentNeumann()
    void solDependentNeumann(PrimaryVariables &values,
                             const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const Intersection &intersection,
                             const int scvIdx,
                             const int boundaryFaceIdx,
                             const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.0;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::sourceAtPos()
   void sourceAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }


    //! \copydoc Dumux::ImplicitProblem::initialAtPos()
     void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0.;

        initial_(values, globalPos);
    }

    // \}

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vertex The vertex
     * \param globalIdx The index of the global vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vertex,
                             const int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return initialPhasePresence_;
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;

        this->model().globalStorage(storage);
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        static Scalar initialWaterContent;
        if (this->timeManager().time() <  this->timeManager().timeStepSize() + 1e-10)
            initialWaterContent = storage[contiWEqIdx];

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            if (this->timeManager().timeStepIndex() % freqMassOutput_ == 0
                || this->timeManager().episodeWillBeOver())
            {
                PrimaryVariables storageChange(0.);
                storageChange = storageLastTimestep_ - storage;

                assert(time - lastMassOutputTime_ != 0);
                storageChange /= (time - lastMassOutputTime_);

                std::cout << "Time[s]: " << time
                          << " TotalMass[kg]: " << storage[contiTotalMassIdx]
                          << " WaterMass[kg]: " << storage[contiWEqIdx]
                          << " IntEnergy[J/m^3]: " << storage[energyEqIdx]
                          << " WaterMassChange[kg/s]: " << storageChange[contiWEqIdx]
                          << std::endl;
                if (this->timeManager().time() != 0.)
                    outfile << time << ";"
                            << storageChange[contiTotalMassIdx] << ";"
                            << storageChange[contiWEqIdx] << ";"
                            << storageChange[energyEqIdx] << ";"
                            << storage[contiWEqIdx] << ";"
                            << initialWaterContent_ - storage[contiWEqIdx] << ";"
                            << storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0])
                            << std::endl;


                static double yMin = 0.0;
                static double yMax = 15.0;
                static std::vector<double> x;
                static std::vector<double> y;

                storageLastTimestep_ = storage;
                lastMassOutputTime_ = time;
                Scalar evaprate = storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0]) * 86400.0; // mm/d
                x.push_back(time / 86400.0); // d
                y.push_back(evaprate);
                yMin = std::min(yMin, evaprate);
                yMax = std::max(yMax, evaprate);

                gnuplot_.reset();
                gnuplot_.setXRange(0, x[x.size()-1]);
                gnuplot_.setYRange(0, yMax+1.1);
                gnuplot_.setXlabel("time [d]");
                gnuplot_.setYlabel("evaporation rate [mm/d]");
                gnuplot_.addFileToPlot("staggered.dat",
                                        "staggered", "w l");
                gnuplot_.addDataSetToPlot(x, y, "box");
                if (GET_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotEvaporationRate))
                {
                    gnuplot_.plot("0_evaprate", true);
                }
            }
        }
    }

private:
    /*!
     * \brief Internal method for the initial condition
     *        (reused for the dirichlet conditions!)
     */
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = pressure_;
        values[switchIdx] = switch_;
        values[temperatureIdx] = temperature_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    static constexpr Scalar eps_ = 1e-8;

    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar darcyXLeft_;
    Scalar darcyXRight_;

    Scalar pressure_;
    Scalar switch_;
    Scalar temperature_;
    int initialPhasePresence_;

    int freqMassOutput_;
    PrimaryVariables storageLastTimestep_;
    Scalar initialWaterContent_;
    Scalar lastMassOutputTime_;
    std::ofstream outfile;
    Dumux::GnuplotInterface<double> gnuplot_;
};
} //end namespace Dumux

#endif // DUMUX_TWOPTWOCNI_SUBPROBLEM_HH
