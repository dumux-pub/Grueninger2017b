// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_WINDTUNNEL_PROBLEM_HH
#define DUMUX_WINDTUNNEL_PROBLEM_HH

#include <dumux/material/fluidsystems/h2oair.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/problem.hh>

#include "windtunneldarcysubproblem.hh"
#include "windtunnelstokessubproblem.hh"

namespace Dumux
{

template <class TypeTag>
class WindtunnelProblem;

template <class TypeTag> class MultiDomainProblem;

namespace Properties
{
// problems
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, Problem,
              Dumux::WindtunnelProblem<TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>);
SET_TYPE_PROP(StokesSubProblem, Problem,
              Dumux::WindtunnelStokesSubProblem<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(DarcySubProblem, Problem,
              Dumux::WindtunnelDarcySubProblem<TTAG(DarcySubProblem)>);

// Set the fluid system to use complex relations (last argument)
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);
#ifdef USE_SIMPLE_PHYSICS
// Set the fluid system to use simple relations but complex Fluid system
SET_TYPE_PROP(DarcySubProblem, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                  TabulatedComponent<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     H2O<typename GET_PROP_TYPE(TypeTag, Scalar)>>,
                                     false>);
// Set the fluid system to use simple relations and simple Fluid system
SET_TYPE_PROP(StokesSubProblem, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                  TabulatedComponent<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     SimpleH2O<typename GET_PROP_TYPE(TypeTag, Scalar)>>,
                                     false>);
#else
SET_TYPE_PROP(DarcySubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), FluidSystem));
SET_TYPE_PROP(StokesSubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), FluidSystem));
#endif

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, UseMoles, false);
SET_BOOL_PROP(DarcySubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), UseMoles));
SET_BOOL_PROP(StokesSubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), UseMoles));
}

template <class TypeTag = TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>
class WindtunnelProblem
: public MultiDomainProblem<TypeTag>
{
    using ParentType = MultiDomainProblem<TypeTag>;

    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

public:
    /*!
     * \brief Base class for the multi domain problem
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The GridView
     */
    WindtunnelProblem(TimeManager &timeManager,
                  GridView gridView)
    : ParentType(timeManager, gridView)
    {
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
    }

private:
};

} // end namespace Dumux

#endif // DUMUX_WINDTUNNEL_PROBLEM_HH
