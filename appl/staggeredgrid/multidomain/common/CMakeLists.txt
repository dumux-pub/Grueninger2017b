
install(FILES
        multidomainccfvelementgeometry.hh
        multidomaincclocalresidual.hh
        multidomainindices.hh
        multidomainlocaloperator.hh
        multidomainnewtonmethod.hh
        multidomainproperties.hh
        multidomainpropertydefaults.hh
        subdomainproperties.hh
        subdomainpropertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/multidomain/common)
