// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * \brief A local operator for PDELab which wraps the Dumux models.
 */

#ifndef DUMUX_MULTIDOMAIN_LOCALOPERATOR_HH
#define DUMUX_MULTIDOMAIN_LOCALOPERATOR_HH

#if ! HAVE_DUNE_PDELAB
#warning "DUNE-PDELab must be available in order to include this file!"
#else

#include <dune/pdelab/gridoperator/common/localmatrix.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dumux/implicit/properties.hh>

namespace Dumux {

namespace Properties
{
NEW_PROP_TAG(DarcySubProblemTypeTag);
NEW_PROP_TAG(NumEq);
}

namespace PDELab {

/*!
 * \brief A local operator for PDELab which wraps the Dumux models.
 */
template<class TypeTag>
class PDELabLocalOperator
    : public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::FullSkeletonPattern,
      public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
    // copying the local operator for PDELab is not a good idea
    PDELabLocalOperator(const PDELabLocalOperator&);

    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVElementGeometry);
    using Model = typename GET_PROP_TYPE(TypeTag, Model);
    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);
    enum { numEq = GET_PROP_VALUE(DarcySubProblemTypeTag, NumEq) };

public:
    // pattern assembly flags
    enum { doPatternVolume = true,
           doPatternSkeleton = true }; // TODO: only for cc, not for box

    // residual assembly flags
    enum { doAlphaVolume = true,
           doAlphaSkeleton = true }; // TODO: only for cc, not for box

    /*!
     * \brief Constructor
     *
     * \param model The physical model for the box scheme.
     */
    PDELabLocalOperator(Model &model)
        : model_(model)
    {}

    /*!
     * \brief Volume integral depending on test and ansatz functions
     *
     * \tparam EG The entity geometry type from PDELab
     * \tparam LFSU The type of the local function space of the ansatz functions
     * \tparam X The type of the container for the coefficients for the ansatz functions
     * \tparam LFSV The type of the local function space of the test functions
     * \tparam R The residual type
     *
     * \param eg The entity geometry object
     * \param lfsu The local function space object of the ansatz functions
     * \param x The object of the container for the coefficients for the ansatz functions
     * \param lfsv The local function space object of the test functions
     * \param r The object storing the volume integral
     */
    template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                      const LFSV& lfsv, R& r) const
    {
        typedef typename LFSU::Traits::SizeType size_type;

        model_.localResidual().eval(eg.entity());

        int numVertices = x.size() / numEq;
        for (size_type dofIdx = 0; dofIdx < r.size(); ++dofIdx)
        {
            r.accumulate(lfsu, dofIdx,
                         model_.localResidual().residual
                         (dofIdx % numVertices)[dofIdx / numVertices]);
        }
    }

    template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_skeleton(const IG& ig,
                        const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                        const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                        R& r_s, R& r_n) const
    {
        // left empty intentionally:
        // entries to residual are already account for in alpha_volume only
        // jacobian_skeleton is needed for the off diagonal matrix entries
    }

    /*!
     * \brief Jacobian of volume term
     *
     * \tparam EG The entity geometry type from PDELab
     * \tparam LFSU The type of the local function space of the ansatz functions
     * \tparam X The type of the container for the coefficients for the ansatz functions
     * \tparam LFSV The type of the local function space of the test functions
     * \tparam Jacobian The Jacobian Matrix type
     *
     * \param eg The entity geometry object
     * \param lfsu The local function space object of the ansatz functions
     * \param x The object of the container for the coefficients for the ansatz functions
     * \param lfsv The local function space object of the test functions
     * \param mat The object accumlating the local jacobian matrices
     */
    template<typename EG, typename LFSU, typename X, typename LFSV, typename Jacobian>
    void jacobian_volume(const EG& eg,
                         const LFSU& lfsu,
                         const X& x,
                         const LFSV& lfsv,
                         Jacobian& mat) const
    {
        typedef typename LFSU::Traits::SizeType size_type;

        model_.localJacobian().assemble(eg.entity());

        int numVertices = x.size() / numEq;

        for (size_type j = 0; j < lfsu.size(); j++)
        {
            for (size_type i = 0; i < lfsu.size(); i++)
            {
                mat.accumulate(lfsu, i, lfsu, j,
                               (model_.localJacobian().mat
                                (i % numVertices, j % numVertices))
                               [i / numVertices][j / numVertices]);
            }
        }
    }

    // jacobian of skeleton term
    template<typename IG, typename LFSU, typename X, typename LFSV, typename M>
    void jacobian_skeleton(const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                           M& mat_ss, M& mat_sn,
                           M& mat_ns, M& mat_nn) const
      {
          typedef typename LFSU::Traits::SizeType size_type;

          FVElementGeometry fvElemGeom_;
          fvElemGeom_.update(model_.gridView(), ig.inside());

          // find fvElemenGeom's neighbor which is the skeleton's outside element
          // initial value not 0, as 0 is the inside dof, followed by neighboring dofs
          unsigned int curNeighbor = 1;
          while ((curNeighbor < fvElemGeom_.numNeighbors)
                && (model_.gridView().grid().hostEntity(ig.outside()) != model_.gridView().grid().hostEntity(fvElemGeom_.neighbors[curNeighbor])))
          {
            ++curNeighbor;
          }

          model_.localJacobian().assemble(ig.inside());

          for (size_type eqSelfIdx = 0; eqSelfIdx < lfsu_s.size(); ++eqSelfIdx)
          {
              for (size_type eqNeigborIdx = 0; eqNeigborIdx < lfsu_n.size(); ++eqNeigborIdx)
              {
                  mat_sn.accumulate(lfsu_s, eqSelfIdx, lfsu_n, eqNeigborIdx,
                                    (model_.localJacobian().mat(0, curNeighbor))
                                    [eqSelfIdx][eqNeigborIdx]);
              }
          }

          // do it a second time in the opposite direction
          fvElemGeom_.update(model_.gridView(), ig.outside());

          // find fvElemenGeom's neighbor which is the skeleton's outside element
          // initial value not 0, as 0 is the inside dof, followed by neighboring dofs
          curNeighbor = 1;
          while ((curNeighbor < fvElemGeom_.numNeighbors)
                && (model_.gridView().grid().hostEntity(ig.inside()) != model_.gridView().grid().hostEntity(fvElemGeom_.neighbors[curNeighbor])))
          {
            ++curNeighbor;
          }

          model_.localJacobian().assemble(ig.outside());

          for (size_type eqSelfIdx = 0; eqSelfIdx < lfsu_s.size(); ++eqSelfIdx)
          {
              for (size_type eqNeigborIdx = 0; eqNeigborIdx < lfsu_n.size(); ++eqNeigborIdx)
              {
                  mat_ns.accumulate(lfsu_n, eqNeigborIdx, lfsu_s, eqSelfIdx,
                                    (model_.localJacobian().mat(0, curNeighbor))
                                    [eqNeigborIdx][eqSelfIdx]);
              }
          }
      }

private:
    Model& model_;
};

/*!
 * \brief An empty local operator for PDELab.
 *
 * The storage term from DuMuX is already included in the Jacobian wrapped
 * by PDELabLocalOperator. Thus is local operator is trivial.
 */
class PDELabInstationaryLocalOperator
    : public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
    // copying the local operator for PDELab is not a good idea
    PDELabInstationaryLocalOperator(const PDELabInstationaryLocalOperator&);

public:
    // default Constructor
    PDELabInstationaryLocalOperator()
    {}

    // pattern assembly flags
    enum { doPatternVolume = true };

    // residual assembly flags
    enum { doAlphaVolume = true };

    // emtpy alpha_volume because storage term is part of the stationary grid operator
    template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                      const LFSV& lfsv, R& r) const
    {}

    // emtpy jacobian_volume because storage term is part of the stationary grid operator
    template<typename EG, typename LFSU, typename X, typename LFSV, typename Jacobian>
    void jacobian_volume(const EG& eg,
                         const LFSU& lfsu,
                         const X& x,
                         const LFSV& lfsv,
                         Jacobian& mat) const
    {}
};

} // namespace PDELab
} // namespace Dumux

#endif // ! HAVE_DUNE_PDELAB

#endif // DUMUX_MULTIDOMAIN_LOCALOPERATOR_HH
