
install(FILES
        basenavierstokesstaggeredgrid.hh
        basenavierstokestransientstaggeredgrid.hh
        navierstokesboundaryconditions.hh
        navierstokesindices.hh
        navierstokesproperties.hh
        navierstokespropertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/freeflow/navierstokes/navierstokes)
