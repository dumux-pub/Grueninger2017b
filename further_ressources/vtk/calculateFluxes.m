% read PVD files for Stokes and Darcy and calculate
% flux in top most and bottom most rows
% for example from Kanschat & Riviere 2010

matSize = 8; % number of horizontal cells
subdomainSize = 0.1; % horizontal and vertical size of subdomains
permeability = 2.65e-10;
mobilityN = 54828.6;

% get pressure from Darcy part at bottom
pathToPVD = 'conservation/8x16/conservation-pm.pvd';
pathToOutput = 'output-darcy.mat';
variableNameList{1} = 'pn_rel';

vtuReader_CellData(pathToPVD,pathToOutput,variableNameList);

load('output-darcy.mat');
lastStep = size(ref_pn_rel,1); % number of steps in PVD file
p = squeeze(ref_pn_rel(lastStep,1,:));
p = flipud(vec2mat(p,matSize));

% get inflow velocity from Stokes part
pathToPVD = 'conservation/8x16/conservation-ff.pvd';
pathToOutput = 'output-stokes.mat';
variableNameList{1} = 'velocityN';

vtuReader_CellData(pathToPVD,pathToOutput,variableNameList);

numberVx = matSize*(matSize/2+1);
numberVy = (matSize+1)*(matSize/2);

load('output-stokes.mat');
v = squeeze(ref_velocityN(lastStep,:,:));
vx = v(1,1:4:size(v,2));
vx = vec2mat(vx,matSize);
% glue additional line on right
vx =[vx, v(1,matSize*4-2:matSize*4:size(v,2))'];
vx = flipud(vx);
vy = v(2,1:4:size(v,2));
vy = vec2mat(vy,matSize);
% glue additional line on bottom
vy =[vy; v(2,size(v,2)-matSize*4+3:4:size(v,2))];
vy = flipud(vy);


% optional output
%imagesc(p);colorbar
%imagesc(vx);colorbar
%imagesc(vy);colorbar

deltaY = subdomainSize / matSize; % subdomain size / number of cells
vDarcy = -(p(matSize/2-1,:)-p(matSize/2,:))/(deltaY)*permeability * mobilityN;
fluxDarcy = sum(vDarcy)/matSize * subdomainSize
fluxStokes = sum(vy(1,:))/matSize * subdomainSize
fluxDiff = abs(fluxDarcy - fluxStokes)

% clean up
delete('output-stokes.mat', 'output-darcy.mat');
