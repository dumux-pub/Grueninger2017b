#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
builddir=/temp/fetzer/dumux-210/dumux-Grueninger2016a/build-gcc/
outdir=$builddir/../results/

# predefined names
executable=windtunnel_box
input=windtunnel.input
sourcedir=$builddir/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test
simdir=$outdir/windtunnel/box_0.1mm

# make executable
cd $sourcedir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ]; then
  exit 1
fi
mkdir -p $simdir

cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells0 32 \
  -Grid.Cells1 '32 32' \
  -Grid.Positions1 '0.0 0.25 0.5' \
  -Grid.Grading1 '-1.21763 1.21763' \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh && chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
