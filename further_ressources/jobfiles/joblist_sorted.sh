sh ./conservation_8x16.sh
sh ./conservation_16x32.sh
sh ./conservation_32x64.sh
sh ./conservation_64x128.sh
sh ./conservation_128x256.sh
sh ./conservation_256x512.sh

sh ./windtunnel_staggered_0.8mm_complexphysics.sh
sh ./windtunnel_staggered_0.4mm_complexphysics.sh
sh ./windtunnel_staggered_0.2mm_complexphysics.sh
sh ./windtunnel_staggered_0.1mm_complexphysics.sh
sh ./windtunnel_staggered_0.05mm_complexphysics.sh
# sh ./windtunnel_staggered_8.0mm_complexphysics.sh

sh ./windtunnel_staggered_0.8mm.sh
sh ./windtunnel_staggered_0.4mm.sh
sh ./windtunnel_staggered_0.2mm.sh
sh ./windtunnel_staggered_0.1mm.sh
sh ./windtunnel_staggered_0.05mm.sh
# sh ./windtunnel_staggered_8.0mm.sh

sh ./windtunnel_box_0.8mm.sh
sh ./windtunnel_box_0.4mm.sh
sh ./windtunnel_box_0.2mm.sh
sh ./windtunnel_box_0.1mm.sh
sh ./windtunnel_box_0.05mm.sh
# sh ./windtunnel_box_8.0mm.sh
