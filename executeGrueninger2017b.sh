#!/usr/bin/env bash

# check argument passed, print help
if [[ $# -ne 1 || $1 == -h || $1 == --help ]]; then
  echo "Add one of the following names as an argument to execute the example you want to run:"
  echo "* test_conservation"
  echo "* test_conservation_box"
  echo "* windtunnel"
  echo "* windtunnel_box"
  echo "* windtunnel3d"
  echo "Check the README.md for a brief explanation for the executables."
  exit
fi

#check for valid argument
if [[ $1 != "test_conservation" && $1 != "test_conservation_box" && $1 != "windtunnel" && $1 != "windtunnel_box" && $1 != "windtunnel3d" ]]; then
  echo "Unknown executable \"$1\". Use -h to get list of executables"
  exit
fi

# build and execute
if [ "$1" == "test_conservation" ]; then
  cd build-cmake
  make test_conservation
  cd ..
  ./build-cmake/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/test_conservation ./appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/test_conservation.input
elif [ "$1" == "test_conservation_box" ]; then
  cd build-cmake
  make test_conservation_box
  cd ..
  ./build-cmake/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/test_conservation_box ./appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/test_conservation.input
elif [ "$1" == "windtunnel" ]; then
  cd build-cmake
  make windtunnel
  cd ..
  ./build-cmake/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel ./appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel.input
elif [ "$1" == "windtunnel_box" ]; then
  cd build-cmake
  make windtunnel_box
  cd ..
  ./build-cmake/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel_box ./appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel.input
elif [ "$1" == "windtunnel3d" ]; then
  cd build-cmake
  make windtunnel3d
  cd ..
  ./build-cmake/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel3d ./appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test/windtunnel3d.input
fi
